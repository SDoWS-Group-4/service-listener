package dtu;

import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.UUID;

import com.google.gson.Gson;

public class ServiceListener {

	MessageQueue queue;

	public ServiceListener(MessageQueue q) {
		this.queue = q;

		this.queue.addHandler("CustomerRegistrationRequested", this::handlePrint);
		this.queue.addHandler("CustomerDeregistrationRequested", this::handlePrint);
        this.queue.addHandler("CustomerByIdRequested", this::handlePrint);
		this.queue.addHandler("CustomerByIdFound", this::handlePrint);
        this.queue.addHandler("CustomerByIdNotFound", this::handlePrint);


		this.queue.addHandler("MerchantByIdFound", this::handlePrint);
        this.queue.addHandler("MerchantByIdNotFound", this::handlePrint);
		this.queue.addHandler("MerchantRegistrationRequested", this::handlePrint);
		this.queue.addHandler("MerchantDeregistrationRequested", this::handlePrint);
		this.queue.addHandler("MerchantByIdRequested", this::handlePrint);

        // Bank Service
		this.queue.addHandler("TransferMoneyRequested", this::handlePrint);
		
		// Account 
		this.queue.addHandler("RegisterAccountRequested", this::handlePrint);
		this.queue.addHandler("RetireAccountRequested", this::handlePrint);
		this.queue.addHandler("RetrieveAccountRequested", this::handlePrint);

		// Payment Manager
		this.queue.addHandler("PaymentRequested", this::handlePrint);

		this.queue.addHandler("MoneyTransfered", this::handlePrint);
        this.queue.addHandler("MoneyNotTransfered", this::handlePrint);
		
		// Token 
		this.queue.addHandler("ValidateToken", this::handlePrint);
        this.queue.addHandler("ConsumeToken", this::handlePrint);
        this.queue.addHandler("GenerateTokenRequest", this::handlePrint);
		this.queue.addHandler("TokenValidated", this::handlePrint);
        this.queue.addHandler("TokenNotValidated", this::handlePrint);
        

	}

	public void handlePrint(Event ev) {
			System.out.println("[x] Trigged Event! : "+ new Gson().toJson(ev));
		
	}
}
